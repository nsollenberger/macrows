import React from "react";
import { render, screen, act } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { MemoryRouter } from "react-router-dom";
import { MainPage } from "./MainPage";

test('renders "Welcome" message', () => {
  render(
    <MemoryRouter>
      <MainPage />
    </MemoryRouter>
  );

  expect(screen.getByText("Welcome to Macrows!")).toBeInTheDocument();
  userEvent.click(screen.getByRole("button", { name: /Continue to App/i }));
  expect(screen.queryByText("Welcome to Macrows!")).not.toBeInTheDocument();
});
