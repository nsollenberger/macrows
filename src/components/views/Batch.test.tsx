import React from "react";
import { v4 as uuidv4 } from "uuid";
import { useForm, useFieldArray } from "react-hook-form";
import { render, screen, act } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { mockRecipes, mockProfiles } from "../../containers/DataProvider";
import { Batch, BatchProps, IBatch } from "./Batch";
import { InitialState, Populated } from "./Batch.stories";
import { Populated as PopulatedOrder } from "./Order.stories";

describe("Batch", () => {
  const basicProps = {
    onCancel: jest.fn(),
    onUpsertContinue: jest.fn(),
    onUpsert: jest.fn(),
    ...InitialState.args,
  } as BatchProps;

  beforeEach(() => {
    jest.clearAllMocks();
  });

  test("renders empty state if there are no unprocessed orders", () => {
    render(<Batch {...basicProps} orders={[]} />);

    expect(
      screen.getByText("There are no unprocessed orders.")
    ).toBeInTheDocument();
  });

  test("filters available orders to show only unprocessed orders", () => {
    render(
      <Batch
        {...basicProps}
        batches={[
          {
            id: uuidv4(),
            created: "1644689673",
            name: "Test Batch",
            orderIds: InitialState.args!.orders!.map((_o) => _o.id),
            completedGrocies: [],
          },
        ]}
      />
    );

    expect(
      screen.getByText("There are no unprocessed orders.")
    ).toBeInTheDocument();
  });

  test("allows selecting orders for processing", async () => {
    render(<Batch {...basicProps} />);

    expect(screen.getByText("Create Batch")).toBeInTheDocument();

    userEvent.click(
      screen.getByRole("checkbox", {
        name: /john h 4\/1\/2022/i,
      })
    );

    await act(async () => {
      userEvent.click(
        screen.getByRole("button", { name: /save and process/i })
      );
      await new Promise((r) => setTimeout(r, 0));
    });

    expect(basicProps.onCancel).not.toBeCalled();
    expect(basicProps.onUpsertContinue).toBeCalledWith<[IBatch]>({
      id: expect.stringMatching(/^[0-9a-f]{8}-/i),
      created: expect.stringMatching(/^\d{10}$/i),
      name: "",
      orderIds: [PopulatedOrder.args!.order!.id],
      completedGrocies: [],
    });

    expect(screen.getByText("Process Batch")).toBeInTheDocument();
    expect(
      screen.queryByRole("button", { name: /save and process/i })
    ).not.toBeInTheDocument();
  });

  test("requires at least 1 order in a batch", async () => {
    render(<Batch {...basicProps} />);

    userEvent.type(
      screen.getByRole("textbox", { name: /batch name/i }),
      "Test batch"
    );
    await act(async () => {
      userEvent.click(
        screen.getByRole("button", { name: /save and process/i })
      );
      await new Promise((r) => setTimeout(r, 0));
    });

    expect(basicProps.onUpsert).not.toBeCalled();
    expect(
      screen.getByText("Batch must contain at least one order")
    ).toBeInTheDocument();
  });

  test("renders pre-filled batch", () => {
    render(<Batch {...basicProps} {...Populated.args} />);

    expect(screen.getByText("Process Batch")).toBeInTheDocument();
    expect(screen.getByText("Included Orders")).toBeInTheDocument();
    expect(screen.getByText("John H 4/1/2022")).toBeInTheDocument();
  });

  test("saves grocery list state", async () => {
    render(<Batch {...basicProps} {...Populated.args} />);

    const firstGroceryItem = screen.getAllByRole<HTMLInputElement>("checkbox", {
      name: /Chicken .+ \(\d servings of .+\)/i,
    });
    userEvent.click(firstGroceryItem[0]);
    await act(async () =>
      userEvent.click(screen.getByRole("button", { name: /save/i }))
    );

    expect(basicProps.onCancel).not.toBeCalled();
    expect(basicProps.onUpsert).toBeCalledWith<[IBatch]>({
      id: expect.stringMatching(/^[0-9a-f]{8}-/i),
      created: PopulatedOrder.args!.order!.created,
      name: "Test batch 1",
      orderIds: [PopulatedOrder.args!.order!.id],
      completedGrocies: [firstGroceryItem[0].value],
    });
  });

  test("allows editing batch", async () => {
    render(<Batch {...basicProps} {...Populated.args} />);

    userEvent.click(screen.getByRole("button", { name: /edit batch/i }));

    expect(screen.getByText("Edit Batch")).toBeInTheDocument();
  });

  test("prevents already-included orders from being duplicated, when editing batch", () => {
    render(
      <Batch
        {...basicProps}
        batch={{
          id: uuidv4(),
          created: "1644690181",
          name: "Test Batch",
          orderIds: InitialState.args!.orders!.map((_o) => _o.id),
          completedGrocies: [],
        }}
        batches={[]} /* Contrived: batch _should_ show up in batches. 
      This test ensures orders are not duplicated, even if current batch is missing from batches list. */
      />
    );

    userEvent.click(screen.getByRole("button", { name: /edit batch/i }));

    expect(
      screen.getAllByRole("checkbox", {
        name: /john h 4\/1\/2022/i,
      }).length
    ).toBe(1);
  });

  test("generates grocery list", () => {
    render(<Batch {...basicProps} {...Populated.args} />);

    expect(screen.getByText("Grocery List")).toBeInTheDocument();
    expect(
      screen.getByText(/chicken - \d{1,}(\.\d)? grams/i)
    ).toBeInTheDocument();
    expect(screen.getByText(/rice - \d{1,}(\.\d)? grams/i)).toBeInTheDocument();
  });

  test("generates prep sheets", () => {
    render(<Batch {...basicProps} {...Populated.args} />);

    expect(screen.getByText("Prep Notes")).toBeInTheDocument();
    expect(
      screen.getByText("John H's Chicken and Rice (COUNT = 3)")
    ).toBeInTheDocument();
    expect(
      screen.getByText(/\d{1,}(\.\d)? grams of chicken/i)
    ).toBeInTheDocument();
    expect(
      screen.getByText(/\d{1,}(\.\d)? grams of rice/i)
    ).toBeInTheDocument();
  });
});
