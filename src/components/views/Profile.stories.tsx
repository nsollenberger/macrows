import React from "react";
import { ComponentStory, ComponentMeta } from "@storybook/react";

import { Profile } from "./Profile";
import { mockProfiles } from "../../containers/DataProvider";

export default {
  title: "Macrows/Views/Profile",
  component: Profile,
} as ComponentMeta<typeof Profile>;

const Template: ComponentStory<typeof Profile> = (args) => (
  <Profile {...args} />
);

export const NewProfile = Template.bind({});
NewProfile.args = {};

export const Populated = Template.bind({});
Populated.args = {
  profile: mockProfiles[0],
};
