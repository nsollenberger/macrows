import React from "react";
import { v4 as uuidv4 } from "uuid";
import { ComponentStory, ComponentMeta } from "@storybook/react";
import { action } from "@storybook/addon-actions";
import { useForm } from "react-hook-form";
import { IngredientInput, Ingredient } from "./IngredientInput";
import { IRecipe } from "./Recipe";
import { ServingUnit } from "../../utils/parse-macros";
import { MockFoodDataSearchProvider } from "../../containers/FoodDataSearch";
import { mockSearchResults } from "../../utils/fooddata-central.mock-data";
import {
  FDSearchStateProvider,
  MockFDSearchStateProvider,
} from "../../containers/FDSearchState";

export default {
  title: "Macrows/Views/Recipe/IngredientInput",
  component: IngredientInput,
  excludeStories: ["withRecipeForm", "initialFormState"],
  argTypes: {
    remove: { action: "useForm.remove" },
    setFocus: { control: false },
    register: { control: false },
    fieldIndex: { control: false },
  },
} as ComponentMeta<typeof IngredientInput>;

export const withRecipeForm =
  <P extends {}>(
    Component: React.ComponentType<P>,
    defaultValues?: Partial<IRecipe>
  ) =>
  (args: Omit<P, "register" | "setFocus" | "watch">) => {
    const { register, setFocus, setValue, getValues, watch } = useForm<IRecipe>(
      {
        defaultValues,
      }
    );
    return (
      <form>
        <Component
          {...(args as P)}
          register={register}
          setFocus={setFocus}
          setValue={setValue}
          getValues={getValues}
          watch={watch}
        />
      </form>
    );
  };

const emptyIngredient = {
  id: uuidv4(),
} as Ingredient;

export const initialFormState = {
  id: "NEW",
  name: "",
  ingredients: [emptyIngredient],
};

const IngredientInputWithForm = withRecipeForm(
  IngredientInput,
  initialFormState
);

export const InitialState: ComponentStory<typeof IngredientInput> = (props) => (
  <MockFoodDataSearchProvider>
    <FDSearchStateProvider>
      <IngredientInputWithForm {...props} />
    </FDSearchStateProvider>
  </MockFoodDataSearchProvider>
);

InitialState.args = {
  fieldIndex: 0,
  ingredient: emptyIngredient,
};

const populatedIngredient = {
  id: uuidv4(),
  name: "Brocolli",
  servingSize: 50,
  servingUnit: ServingUnit.Grams,
  nutritionFacts: {
    fat: 0,
    protein: 1,
    carbs: 2,
  },
  autoScale: true,
};

const IngredientInputWithFormPopulated = withRecipeForm(IngredientInput, {
  ingredients: [populatedIngredient],
});

export const Populated: ComponentStory<typeof IngredientInput> = (props) => (
  <MockFoodDataSearchProvider>
    <FDSearchStateProvider>
      <IngredientInputWithFormPopulated {...props} />
    </FDSearchStateProvider>
  </MockFoodDataSearchProvider>
);

Populated.args = {
  fieldIndex: 0,
  ingredient: populatedIngredient,
};

const partiallyPopulatedIngredient = {
  ...emptyIngredient,
  name: "cheese",
};

const IngredientInputWithFormPartiallyPopulated = withRecipeForm(
  IngredientInput,
  {
    ingredients: [partiallyPopulatedIngredient],
  }
);

export const StaticWithSearchPending: ComponentStory<typeof IngredientInput> = (
  props
) => (
  <MockFoodDataSearchProvider>
    <MockFDSearchStateProvider
      value={{ isSearching: true, abort: action("abort") }}
    >
      <IngredientInputWithFormPartiallyPopulated {...props} />
    </MockFDSearchStateProvider>
  </MockFoodDataSearchProvider>
);

StaticWithSearchPending.args = {
  fieldIndex: 0,
  ingredient: emptyIngredient,
};

const _search = (...args: any) => {
  action("search")(args);
  return Promise.resolve(undefined);
};

export const StaticWithSearchResults: ComponentStory<typeof IngredientInput> = (
  props
) => (
  <MockFoodDataSearchProvider>
    <MockFDSearchStateProvider
      value={{
        searchResults: mockSearchResults,
        clearResults: action("clearResults"),
        search: _search,
      }}
    >
      <IngredientInputWithFormPartiallyPopulated {...props} />
    </MockFDSearchStateProvider>
  </MockFoodDataSearchProvider>
);

StaticWithSearchResults.args = {
  fieldIndex: 0,
  ingredient: emptyIngredient,
};
