import React from "react";
import { ComponentStory, ComponentMeta } from "@storybook/react";

import { Recipe } from "./Recipe";
import { mockRecipes } from "../../containers/DataProvider";
import { MockFoodDataSearchProvider } from "../../containers/FoodDataSearch";

export default {
  title: "Macrows/Views/Recipe",
  component: Recipe,
} as ComponentMeta<typeof Recipe>;

const Template: ComponentStory<typeof Recipe> = (props) => (
  <MockFoodDataSearchProvider>
    <Recipe {...props} />
  </MockFoodDataSearchProvider>
);

export const NewRecipe = Template.bind({});
NewRecipe.args = {};

export const Populated = Template.bind({});
Populated.args = {
  recipe: mockRecipes[0],
};
