import React from "react";
import { v4 as uuidv4 } from "uuid";
import { useForm } from "react-hook-form";
import { ComponentStory, ComponentMeta } from "@storybook/react";
import { CustomizedMeal } from "./CustomizedMeal";
import { mockRecipes, mockProfiles } from "../../containers/DataProvider";
import { IOrder } from "./Order";

export const withOrderForm =
  <P extends {}>(
    Component: React.ComponentType<P>,
    defaultValues?: Partial<IOrder>
  ) =>
  (args: Omit<P, "register" | "watch">) => {
    const { register, watch } = useForm<IOrder>({ defaultValues });
    return <Component {...(args as P)} register={register} watch={watch} />;
  };

export default {
  title: "Macrows/Views/Order/CustomizedMeal",
  component: CustomizedMeal,
  excludeStories: ["withOrderForm", "initialFormState"],
  argTypes: {
    remove: { action: "useForm.remove" },
  },
} as ComponentMeta<typeof CustomizedMeal>;

export const initialFormState = {
  meals: [
    {
      id: uuidv4(),
      recipeId: mockRecipes[0].id,
      profileId: mockProfiles[0].id,
      targetCals: 650,
      quantity: 1,
      ingredients: [],
    },
  ],
};

export const InitialState: ComponentStory<typeof CustomizedMeal> =
  withOrderForm(CustomizedMeal, initialFormState);

InitialState.args = {
  recipe: mockRecipes[0],
  profile: mockProfiles[0],
};

export const Populated: ComponentStory<typeof CustomizedMeal> = withOrderForm(
  CustomizedMeal,
  {
    meals: [
      {
        id: uuidv4(),
        profileId: mockProfiles[0].id,
        recipeId: mockRecipes[0].id,
        targetCals: 650,
        quantity: 4,
        ingredients: mockRecipes[0].ingredients.map((_i, index) => ({
          id: _i.id,
          amount: _i.servingSize * 2,
        })),
      },
    ],
  }
);

Populated.args = {
  recipe: mockRecipes[0],
  profile: mockProfiles[0],
};
