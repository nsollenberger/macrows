import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { Recipe, IRecipe } from "./Recipe";
import { mockRecipes } from "../../containers/DataProvider";

describe("Recipe", () => {
  const basicProps = {
    onCancel: jest.fn(),
    onUpsert: jest.fn(),
  };

  beforeEach(() => {
    jest.clearAllMocks();
  });

  test("renders empty state", () => {
    render(<Recipe {...basicProps} />);
    const titleEl = screen.getByText("Add New Recipe");
    expect(titleEl).toBeInTheDocument();

    expect(basicProps.onCancel).not.toHaveBeenCalled();
    expect(basicProps.onUpsert).not.toHaveBeenCalled();
  });

  test("includes ingredient.id field", async () => {
    render(<Recipe {...basicProps} />);

    fireEvent.change(screen.getByRole("textbox", { name: /recipe name/i }), {
      target: { value: "Cup of Sauce" },
    });
    fireEvent.click(screen.getByRole("button", { name: /add ingredient/i }));
    fireEvent.change(
      screen.getByRole("textbox", { name: /ingredient name/i }),
      { target: { value: "Special sauce" } }
    );
    fireEvent.change(
      screen.getByRole("spinbutton", { name: /serving size/i }),
      { target: { value: "4.5" } }
    );
    fireEvent.change(screen.getByRole("spinbutton", { name: /g fat/i }), {
      target: { value: "6" },
    });
    fireEvent.click(screen.getByRole("button", { name: /save/i }));
    await new Promise((r) => setTimeout(r, 0));

    expect(basicProps.onUpsert).toHaveBeenCalledWith<[IRecipe]>({
      id: expect.stringMatching(/^[0-9a-f]{8}-/i),
      created: expect.stringMatching(/^\d{10}$/),
      name: "Cup of Sauce",
      ingredients: [
        expect.objectContaining({
          id: expect.stringMatching(/^[0-9a-f]{8}-/i),
          name: "Special sauce",
          servingSize: 4.5,
          nutritionFacts: {
            carbs: 0,
            fat: 6,
            protein: 0,
          },
        }),
      ],
    });
    expect(basicProps.onCancel).not.toHaveBeenCalled();
  });

  test("allows removing ingredients", async () => {
    render(<Recipe {...basicProps} recipe={mockRecipes[0]} />);

    const removeButtonEl = screen.getAllByRole("button", {
      name: /remove ingredient/i,
    });
    expect(removeButtonEl).toHaveLength(2);
    expect(removeButtonEl[0]).toBeInTheDocument();
    userEvent.click(removeButtonEl[0]);

    userEvent.click(screen.getByRole("button", { name: "Save" }));
    await new Promise((r) => setTimeout(r, 0));

    expect(basicProps.onUpsert).toHaveBeenCalledWith<[IRecipe]>({
      id: mockRecipes[0].id,
      created: mockRecipes[0].created,
      name: mockRecipes[0].name,
      ingredients: mockRecipes[0].ingredients.slice(1),
    });
    expect(basicProps.onCancel).not.toHaveBeenCalled();
  });
});
