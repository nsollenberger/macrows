import React from "react";
import { v4 as uuidv4 } from "uuid";
import { ComponentStory, ComponentMeta } from "@storybook/react";

import { Order } from "./Order";
import "../../App.css";
import "../../utils/utils.css";
import { mockRecipes, mockProfiles } from "../../containers/DataProvider";

export default {
  title: "Macrows/Views/Order",
  component: Order,
} as ComponentMeta<typeof Order>;

const Template: ComponentStory<typeof Order> = (args) => <Order {...args} />;

export const NewOrder = Template.bind({});
NewOrder.args = {
  recipes: mockRecipes,
  profiles: mockProfiles,
};

export const Populated = Template.bind({});
Populated.args = {
  order: {
    id: uuidv4(),
    created: "1644690181",
    name: "John H 4/1/2022",
    meals: [
      {
        id: uuidv4(),
        profileId: mockProfiles[0].id,
        targetCals: 555,
        recipeId: mockRecipes[0].id,
        quantity: 3,
        ingredients: mockRecipes[0].ingredients.map((_i) => ({
          id: _i.id,
          amount: _i.servingSize * 2.0,
        })),
      },
      {
        id: uuidv4(),
        profileId: mockProfiles[1].id,
        targetCals: 366,
        recipeId: mockRecipes[1].id,
        quantity: 2,
        ingredients: mockRecipes[1].ingredients.map((_i) => ({
          id: _i.id,
          amount: _i.servingSize * 2.5,
        })),
      },
    ],
  },
  recipes: mockRecipes,
  profiles: mockProfiles,
};
