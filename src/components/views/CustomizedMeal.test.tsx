import React from "react";
import { v4 as uuidv4 } from "uuid";
import { useForm, useFieldArray } from "react-hook-form";
import { render, screen, fireEvent } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { mockRecipes, mockProfiles } from "../../containers/DataProvider";
import { CustomizedMeal, CustomizedMealInputProps } from "./CustomizedMeal";
import {
  withOrderForm,
  initialFormState,
  InitialState,
} from "./CustomizedMeal.stories";

jest.mock("../primitives/CircularRange");

describe("CustomizedMeal", () => {
  const basicProps = InitialState.args as CustomizedMealInputProps;
  const CustomizedMealWithForm = withOrderForm(
    CustomizedMeal,
    initialFormState
  );

  test("renders error state if missing recipe or profile details", () => {
    render(<CustomizedMealWithForm fieldIndex={0} remove={() => {}} />);
    const placeholderEl = screen.getByText("Error: missing meal data");
    expect(placeholderEl).toBeInTheDocument();
  });

  test("renders title with recipe and profile name", () => {
    render(<CustomizedMealWithForm {...basicProps} />);
    const titleEl = screen.getByText("John H's Chicken and Rice");
    expect(titleEl).toBeInTheDocument();
  });

  test("allows specifying number of meals", () => {
    render(<CustomizedMealWithForm {...basicProps} />);
    const mealQuantityEl = screen.getByLabelText(/quantity/i);
    expect(mealQuantityEl).toBeInTheDocument();
  });

  test("renders each ingredient", () => {
    render(<CustomizedMealWithForm {...basicProps} />);
    ["Chicken", "Rice"].forEach((value) => {
      const ingredientEl = screen.getByText(`Ingredient: ${value}`);
      expect(ingredientEl).toBeInTheDocument();
    });
  });

  test("renders a sum of macros", () => {
    render(<CustomizedMealWithForm {...basicProps} />);
    ["fat", "protein", "carbs"].forEach((_m) => {
      const macroSum = screen.getByRole("cell", { name: `grams of ${_m}` });
      expect(macroSum).toBeInTheDocument();
      expect(macroSum.textContent).not.toEqual("0g");
    });
  });

  test("...as a percentage of the profile's nutrient goals", () => {
    render(<CustomizedMealWithForm {...basicProps} />);
    ["fat", "protein", "carbs"].forEach((_m) => {
      const macroPercent = screen.getByRole("cell", {
        name: `percent of target ${_m}`,
      });
      expect(macroPercent).toBeInTheDocument();
      expect(macroPercent.textContent).not.toEqual("");
      expect(macroPercent.textContent).not.toEqual("0%");
    });
  });

  test("allows adjusting ingredient amounts", () => {
    render(<CustomizedMealWithForm {...basicProps} />);

    const defaultPercent = screen.getByRole("cell", {
      name: `percent of target protein`,
    }).textContent;
    const defaultValue = screen.getByRole("cell", {
      name: `grams of protein`,
    }).textContent;
    const initialCarbs = screen.getByRole("cell", {
      name: "grams of carbs",
    }).textContent;

    fireEvent.change(
      screen.getByRole("slider", { name: /ingredient: chicken/i }),
      { target: { value: 20 } }
    );

    const newPercent = screen.getByRole("cell", {
      name: `percent of target protein`,
    }).textContent;
    const newValue = screen.getByRole("cell", {
      name: `grams of protein`,
    }).textContent;
    const newCarbs = screen.getByRole("cell", {
      name: "grams of carbs",
    }).textContent;

    expect(newPercent).not.toEqual(defaultPercent);
    expect(newValue).not.toEqual(defaultValue);
    expect(newCarbs).toEqual(initialCarbs);
  });
});
