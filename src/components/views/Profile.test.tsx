import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { Profile } from "./Profile";

jest.mock("../primitives/PieChart");

describe("Profile", () => {
  const basicProps = {
    onCancel: jest.fn(),
    onUpsert: jest.fn(),
  };

  test("renders empty state", () => {
    render(<Profile {...basicProps} />);
    const titleEl = screen.getByText("Add New Profile");
    expect(titleEl).toBeInTheDocument();

    expect(screen.getByRole("textbox", { name: /name/i })).toBeInTheDocument();
  });

  test("allows entering daily target amounts in grams", () => {
    render(<Profile {...basicProps} />);

    const proteinInput = screen.getByRole<HTMLInputElement>("spinbutton", {
      name: /protein/i,
    });
    userEvent.type(proteinInput, "{backspace}100");
    expect(proteinInput.value).toBe("100");

    userEvent.type(
      screen.getByRole("spinbutton", { name: /fat/i }),
      "{backspace}40"
    );
    userEvent.type(
      screen.getByRole("spinbutton", { name: /carbs/i }),
      "{backspace}120"
    );
  });

  test("renders macro ratios by grams + calories", () => {
    render(<Profile {...basicProps} />);

    userEvent.type(
      screen.getByRole("spinbutton", { name: /protein/i }),
      "{backspace}100"
    );
    userEvent.type(
      screen.getByRole("spinbutton", { name: /fat/i }),
      "{backspace}40"
    );
    userEvent.type(
      screen.getByRole("spinbutton", { name: /carbs/i }),
      "{backspace}120"
    );

    screen.findByText("Ratio of Calories\n32%, 29%, 39%");
    screen.findByText("Total: 1240 Calories");
  });
});
