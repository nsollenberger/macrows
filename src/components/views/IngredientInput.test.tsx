import { act, render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import React from "react";
import { IngredientInput, IngredientInputProps } from "./IngredientInput";
import {
  initialFormState,
  InitialState,
  withRecipeForm,
} from "./IngredientInput.stories";
import { MockFoodDataSearchProvider } from "../../containers/FoodDataSearch";
import { FDSearchStateProvider } from "../../containers/FDSearchState";

describe("IngredientInput", () => {
  const basicProps = InitialState.args as IngredientInputProps;
  const IngredientInputWithForm = withRecipeForm(
    IngredientInput,
    initialFormState
  );

  const IngredientInputWithContainers: React.FC<
    React.ComponentProps<typeof IngredientInputWithForm>
  > = (props) => (
    <MockFoodDataSearchProvider>
      <FDSearchStateProvider>
        <IngredientInputWithForm {...props} />
      </FDSearchStateProvider>
    </MockFoodDataSearchProvider>
  );

  const cheeseDescription = "CHEDDAR CHEESE (CRYSTAL FARMS brand)";

  test("renders empty state", () => {
    render(<IngredientInputWithContainers {...basicProps} />);

    const nameInput = screen.getByRole("textbox", { name: "Ingredient Name" });
    expect(nameInput).toBeInTheDocument();

    expect(
      screen.getByRole<HTMLInputElement>("checkbox", { name: /auto-scale/i })
    ).not.toBeChecked();
  });

  test("renders search results", async () => {
    render(<IngredientInputWithContainers {...basicProps} />);

    userEvent.type(
      screen.getByRole("textbox", { name: "Ingredient Name" }),
      "Cheese"
    );
    await act(async () =>
      userEvent.click(screen.getByRole("button", { name: "Search" }))
    );

    expect(
      screen.getByRole("list", { name: "Search results" })
    ).toBeInTheDocument();
    expect(screen.getByText(cheeseDescription)).toBeInTheDocument();
  });

  test("populates fields when search result is selected", async () => {
    render(<IngredientInputWithContainers {...basicProps} />);

    userEvent.type(
      screen.getByRole("textbox", { name: "Ingredient Name" }),
      "Cheese"
    );
    await act(async () =>
      userEvent.click(screen.getByRole("button", { name: "Search" }))
    );
    await act(async () => userEvent.click(screen.getByText(cheeseDescription)));

    const nameEl = screen.getByRole<HTMLInputElement>("textbox", {
      name: /ingredient name/i,
    });
    expect(nameEl.value).toBe("CHEDDAR CHEESE");
    const sizeEl = screen.getByRole<HTMLInputElement>("spinbutton", {
      name: /serving size/i,
    });
    expect(sizeEl.value).toBe("100");
    const fatEl = screen.getByRole<HTMLInputElement>("spinbutton", {
      name: /g fat/i,
    });
    expect(fatEl.value).toBe("32.1");
    const carbsEl = screen.getByRole<HTMLInputElement>("spinbutton", {
      name: /g carbs/i,
    });
    expect(carbsEl.value).toBe("3.57");

    expect(screen.queryByText(cheeseDescription)).not.toBeInTheDocument();
  });

  test("defaults auto-scale on when using search result", async () => {
    render(<IngredientInputWithContainers {...basicProps} />);

    userEvent.type(
      screen.getByRole("textbox", { name: "Ingredient Name" }),
      "Cheese"
    );
    await act(async () =>
      userEvent.click(screen.getByRole("button", { name: "Search" }))
    );
    await act(async () => userEvent.click(screen.getByText(cheeseDescription)));

    expect(
      screen.getByRole<HTMLInputElement>("checkbox", { name: /auto-scale/i })
    ).toBeChecked();
  });

  describe("with auto-scale on", () => {
    test("does not allow editing nutrition facts directly", async () => {
      render(<IngredientInputWithContainers {...basicProps} />);

      userEvent.type(
        screen.getByRole("textbox", { name: "Ingredient Name" }),
        "Cheese"
      );
      await act(async () =>
        userEvent.click(screen.getByRole("button", { name: "Search" }))
      );
      await act(async () =>
        userEvent.click(screen.getByText(cheeseDescription))
      );

      // Enable 'auto-scale' if not already
      const autoScaleEl = screen.getByRole<HTMLInputElement>("checkbox", {
        name: /auto-scale/i,
      });
      if (!autoScaleEl.checked) {
        userEvent.click(autoScaleEl);
      }

      const fatEl = screen.getByRole<HTMLInputElement>("spinbutton", {
        name: /g fat/i,
      });
      expect(fatEl).toBeDisabled();
      const carbsEl = screen.getByRole<HTMLInputElement>("spinbutton", {
        name: /g carbs/i,
      });
      expect(carbsEl).toBeDisabled();
    });

    test("updates nutrition facts when serving size is changed", async () => {
      render(<IngredientInputWithContainers {...basicProps} />);

      userEvent.type(
        screen.getByRole("textbox", { name: "Ingredient Name" }),
        "Cheese"
      );
      await act(async () =>
        userEvent.click(screen.getByRole("button", { name: "Search" }))
      );
      await act(async () =>
        userEvent.click(screen.getByText(cheeseDescription))
      );

      // Enable 'auto-scale' if not already
      const autoScaleEl = screen.getByRole<HTMLInputElement>("checkbox", {
        name: /auto-scale/i,
      });
      if (!autoScaleEl.checked) {
        userEvent.click(autoScaleEl);
      }

      const sizeEl = screen.getByRole<HTMLInputElement>("spinbutton", {
        name: /serving size/i,
      });
      expect(sizeEl.value).toBe("100");
      const proteinEl = screen.getByRole<HTMLInputElement>("spinbutton", {
        name: /g protein/i,
      });
      expect(proteinEl.value).toBe("25");
      const carbsEl = screen.getByRole<HTMLInputElement>("spinbutton", {
        name: /g carbs/i,
      });
      expect(carbsEl.value).toBe("3.57");

      userEvent.clear(sizeEl);
      await act(async () => userEvent.type(sizeEl, "42"));
      expect(sizeEl.value).toBe("42");
      expect(proteinEl.value).toBe("10.5");
      expect(carbsEl.value).toBe("1.5");

      userEvent.clear(sizeEl);
      await act(async () => userEvent.type(sizeEl, "80"));
      expect(sizeEl.value).toBe("80");
      expect(proteinEl.value).toBe("20");
      expect(carbsEl.value).toBe("2.86");
    });
  });

  describe("with auto-scale off", () => {
    test("does not update nutrition facts when serving size is changed", async () => {
      render(<IngredientInputWithContainers {...basicProps} />);

      userEvent.type(
        screen.getByRole("textbox", { name: "Ingredient Name" }),
        "Cheese"
      );
      await act(async () =>
        userEvent.click(screen.getByRole("button", { name: "Search" }))
      );
      await act(async () =>
        userEvent.click(screen.getByText(cheeseDescription))
      );

      // Enable 'auto-scale' if not already
      const autoScaleEl = screen.getByRole<HTMLInputElement>("checkbox", {
        name: /auto-scale/i,
      });
      if (autoScaleEl.checked) {
        userEvent.click(autoScaleEl);
      }

      const sizeEl = screen.getByRole<HTMLInputElement>("spinbutton", {
        name: /serving size/i,
      });
      expect(sizeEl.value).toBe("100");
      const proteinEl = screen.getByRole<HTMLInputElement>("spinbutton", {
        name: /g protein/i,
      });
      expect(proteinEl.value).toBe("25");
      const carbsEl = screen.getByRole<HTMLInputElement>("spinbutton", {
        name: /g carbs/i,
      });
      expect(carbsEl.value).toBe("3.57");

      userEvent.clear(sizeEl);
      await act(async () => userEvent.type(sizeEl, "42"));
      expect(sizeEl.value).toBe("42");
      expect(proteinEl.value).toBe("25");
      expect(carbsEl.value).toBe("3.57");
    });

    test("allows non-whole numbers to 2 decimal places", () => {
      render(<IngredientInputWithContainers {...basicProps} />);

      const sizeEl = screen.getByRole("spinbutton", { name: /serving size/i });
      userEvent.type(sizeEl, "2.5");
      expect(sizeEl).toBeValid();

      const fatEl = screen.getByRole("spinbutton", { name: /g fat/i });
      userEvent.type(fatEl, ".5");
      expect(fatEl).toBeValid();

      const carbsEl = screen.getByRole("spinbutton", { name: /g carbs/i });
      userEvent.type(carbsEl, "0.009");
      expect(carbsEl).toBeInvalid();
      userEvent.clear(carbsEl);
      userEvent.type(carbsEl, "0.01");
      expect(carbsEl).toBeValid();
    });
  });
});
