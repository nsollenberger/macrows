import React from "react";
import { v4 as uuidv4 } from "uuid";
import { ComponentStory, ComponentMeta } from "@storybook/react";

import { Batch, BatchProps } from "./Batch";
import { mockRecipes, mockProfiles } from "../../containers/DataProvider";
import { Populated as PopulatedOrder } from "./Order.stories";

export default {
  title: "Macrows/Views/Batch",
  component: Batch,
  argTypes: {
    onCancel: { action: "onCancel" },
    onUpsertContinue: { action: "onUpsertContinue" },
    onUpsert: { action: "onUpsert" },
  },
} as ComponentMeta<typeof Batch>;

const BatchWithState: React.FC<BatchProps> = (args) => {
  const [batchState, setBatchState] = React.useState(args.batch);
  // The Batch component manages unsaved changes (form state), but is a controlled component
  // regarding persistent changes to `batch`.
  return (
    <Batch
      {...args}
      onUpsertContinue={(_b) => {
        setBatchState(_b);
        args.onUpsert(_b);
      }}
      onUpsert={(_b) => {
        setBatchState(_b);
        args.onUpsert(_b);
      }}
      batch={batchState}
    />
  );
};

const Template: ComponentStory<typeof Batch> = (args) => (
  <BatchWithState {...args} />
);

export const EmptyState = Template.bind({});
EmptyState.args = {};

const mockBatch = {
  id: uuidv4(),
  created: "1644690181",
  name: "Test batch 1",
  orderIds: [PopulatedOrder!.args!.order!.id],
  completedGrocies: [],
};

export const InitialState = Template.bind({});
InitialState.args = {
  batch: undefined,
  batches: [],
  orders: [PopulatedOrder!.args!.order!],
  recipes: mockRecipes,
  profiles: mockProfiles,
};

export const Populated = Template.bind({});
Populated.args = {
  batch: mockBatch,
  batches: [mockBatch],
  orders: [PopulatedOrder!.args!.order!],
  recipes: mockRecipes,
  profiles: mockProfiles,
};
