import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { Order, IOrder } from "./Order";
import { mockRecipes, mockProfiles } from "../../containers/DataProvider";
import { Populated } from "./Order.stories";

jest.mock("../primitives/CircularRange");

describe("Order", () => {
  const basicProps = {
    onCancel: jest.fn(),
    onUpsert: jest.fn(),
    recipes: mockRecipes,
    profiles: mockProfiles,
  };

  beforeEach(() => {
    jest.clearAllMocks();
  });

  test("renders empty state", () => {
    render(<Order {...basicProps} />);
    const titleEl = screen.getByText("Add New Order");
    expect(titleEl).toBeInTheDocument();

    expect(basicProps.onCancel).not.toHaveBeenCalled();
    expect(basicProps.onUpsert).not.toHaveBeenCalled();
  });

  test("includes meals[].id and meals.ingredients[].id fields in upsert", async () => {
    render(<Order {...basicProps} />);

    userEvent.type(
      screen.getByRole("textbox", { name: /order name/i }),
      "Test order"
    );
    userEvent.click(screen.getByRole("button", { name: /add to order/i }));
    fireEvent.change(
      screen.getByRole("slider", { name: /ingredient: chicken/i }),
      { target: { value: 50 } }
    );
    userEvent.click(screen.getByRole("button", { name: /save/i }));
    await new Promise((r) => setTimeout(r, 0));

    expect(basicProps.onCancel).not.toHaveBeenCalled();
    expect(basicProps.onUpsert).toHaveBeenCalledTimes(1);
    expect(basicProps.onUpsert).toHaveBeenCalledWith<[IOrder]>({
      id: expect.stringMatching(/^[0-9a-f]{8}-/i),
      created: expect.stringMatching(/^\d{10}$/i),
      name: "Test order",
      meals: [
        expect.objectContaining<Partial<IOrder["meals"][0]>>({
          id: expect.stringMatching(/^[0-9a-f]{8}-/i),
          recipeId: mockRecipes[0].id,
          profileId: mockProfiles[0].id,
          quantity: 1,
          ingredients: [
            { id: mockRecipes[0].ingredients[0].id, amount: 50 },
            { id: mockRecipes[0].ingredients[1].id, amount: 100 },
          ],
        }),
      ],
    });
  });

  test("allows removing CustomizedMeals", async () => {
    render(<Order {...basicProps} {...Populated.args} />);
    const removeButtonEl = screen.getAllByRole("button", {
      name: /remove meal/i,
    });
    expect(removeButtonEl).toHaveLength(2);
    expect(removeButtonEl[0]).toBeInTheDocument();
    userEvent.click(removeButtonEl[0]);

    userEvent.click(screen.getByRole("button", { name: "Save" }));
    await new Promise((r) => setTimeout(r, 0));

    expect(basicProps.onUpsert).toHaveBeenCalledWith<[IOrder]>({
      id: Populated.args!.order!.id,
      created: Populated.args!.order!.created,
      name: Populated.args!.order!.name,
      meals: Populated.args!.order!.meals.slice(1),
    });
  });
});
