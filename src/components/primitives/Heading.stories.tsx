import React from "react";
import { ComponentStory, ComponentMeta } from "@storybook/react";

import { Heading, SubHeading } from "./Heading";

export default {
  title: "Macrows/Primitives/Heading",
  component: Heading,
  argTypes: {
    children: { control: false },
  },
} as ComponentMeta<typeof Heading>;

const Template: ComponentStory<typeof Heading> = (args) => (
  <div>
    <h1>Title</h1>
    <Heading {...args} />
    <p>
      This is a paragraph of words. Perhaps you have read words before, but have
      you read these words?
    </p>
    <SubHeading>Sub-heading</SubHeading>
    <p>More text for demonstration purposes. Did you catch the typo?</p>
  </div>
);

export const Default = Template.bind({});
Default.args = {
  children: "Heading",
};
