import React from "react";
import { ComponentStory, ComponentMeta } from "@storybook/react";

import { ToggleButton, ToggleButtonOption } from "./ToggleButton";

export default {
  title: "Macrows/Primitives/ToggleButton",
  component: ToggleButton,
  subcomponents: { ToggleButtonOption },
} as ComponentMeta<typeof ToggleButton>;

const Template: ComponentStory<typeof ToggleButton> = (args) => {
  const [selected, onToggle] = React.useState<string>();
  return (
    <ToggleButton {...args} {...{ selected, onToggle }}>
      <ToggleButtonOption label="First item" value="option_1" />
      <ToggleButtonOption label="Second item" value="option_2" />
      <ToggleButtonOption label="Third item" value="option_3" />
    </ToggleButton>
  );
};

export const Default = Template.bind({});

export const AcceptsButtonProps = Template.bind({});
AcceptsButtonProps.args = {
  square: true,
  size: "small",
};
