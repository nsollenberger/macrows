import React from "react";
import { ComponentStory, ComponentMeta } from "@storybook/react";
import { action } from "@storybook/addon-actions";

import { MenuButton, MenuButtonOption } from "./MenuButton";

export default {
  title: "Macrows/Primitives/MenuButton",
  component: MenuButton,
  subcomponents: { MenuButtonOption },
} as ComponentMeta<typeof MenuButton>;

export const Populated: ComponentStory<typeof MenuButton> = (args) => (
  <MenuButton {...args}>
    <MenuButtonOption
      label="First item"
      onClick={action("MenuButtonOption.onClick")}
    />
    <MenuButtonOption
      label="Second item"
      onClick={action("MenuButtonOption.onClick")}
    />
    <MenuButtonOption
      label="Third item"
      onClick={action("MenuButtonOption.onClick")}
    />
  </MenuButton>
);

Populated.args = {
  label: "Menu Label",
};
