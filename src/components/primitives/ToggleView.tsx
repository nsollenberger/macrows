import React from "react";

export const ToggleView: React.FC<{
  a: any;
  b: any;
  toggle: boolean;
}> = ({ a, b, toggle }) => {
  return toggle ? b : a;
};
