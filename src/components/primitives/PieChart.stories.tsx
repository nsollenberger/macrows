import React from "react";
import { ComponentStory, ComponentMeta } from "@storybook/react";
import { PieChart } from "./PieChart";

export default {
  title: "Macrows/Primitives/PieChart",
  component: PieChart,
} as ComponentMeta<typeof PieChart>;

const Template: ComponentStory<typeof PieChart> = (args) => (
  <PieChart {...args} />
);

export const WithData = Template.bind({});
WithData.args = { values: [10, 10, 10] };

export const LotsOfData = Template.bind({});
LotsOfData.args = { values: [10, 10, 10, 15, 5, 2] };

export const EmptyState = Template.bind({});
EmptyState.args = {};
