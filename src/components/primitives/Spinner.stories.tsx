import React from "react";
import { ComponentStory, ComponentMeta } from "@storybook/react";
import { Spinner } from "./Spinner";
import { Button } from "./Button";

export default {
  title: "Macrows/Primitives/Spinner",
  component: Spinner,
} as ComponentMeta<typeof Spinner>;

const Template: ComponentStory<typeof Spinner> = (args) => (
  <Spinner {...args} />
);

export const Defalt = Template.bind({});
Defalt.args = {
  active: true,
};

export const Togglable: ComponentStory<typeof Spinner> = (args) => {
  const [isActive, setActive] = React.useState(false);
  return (
    <>
      <Button label="Toggle Active" onClick={() => setActive((_a) => !_a)} />
      <Spinner {...args} active={isActive} />
    </>
  );
};
