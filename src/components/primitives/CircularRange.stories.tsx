import React from "react";
import { ComponentStory, ComponentMeta } from "@storybook/react";
import { CircularRange } from "./CircularRange";

export default {
  title: "Macrows/Primitives/CircularRange",
  component: CircularRange,
} as ComponentMeta<typeof CircularRange>;

const Template: ComponentStory<typeof CircularRange> = (args) => (
  <CircularRange {...args} />
);

export const EmptyState = Template.bind({});
EmptyState.args = {};

export const Partial = Template.bind({});
Partial.args = { value: 50, max: 100 };

export const Full = Template.bind({});
Full.args = { value: 100, max: 100 };

export const OverFull = Template.bind({});
OverFull.args = { value: 150, max: 100 };

export const Adjustable: ComponentStory<typeof CircularRange> = (args) => {
  const [value, setValue] = React.useState(25);
  return (
    <div>
      <CircularRange {...args} value={value} />
      <br />
      <input
        type="range"
        value={value}
        onChange={(e) => setValue(Number(e.target.value))}
      />
    </div>
  );
};

export const CustomRadius = Template.bind({});
CustomRadius.args = { value: 95, max: 100, radius: 50 };
