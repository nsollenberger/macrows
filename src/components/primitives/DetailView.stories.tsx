import React from "react";
import { MemoryRouter } from "react-router-dom";
import { ComponentStory, ComponentMeta } from "@storybook/react";

import { Button } from "./Button";
import { DetailView, useDetailView } from "./DetailView";

export default {
  title: "Macrows/Primitives/DetailView",
  component: DetailView,
  argTypes: {
    children: { control: false },
  },
} as ComponentMeta<typeof DetailView>;

const Template: ComponentStory<typeof DetailView> = (args) => (
  <MemoryRouter>
    <DetailView {...args} />
    <h1>Background Content</h1>
    <p>
      This is a paragraph of words. Perhaps you have read words before, but have
      you read these words?
    </p>
    <p>
      Background text is in the background doing background text things. You
      aren't missing much if you skip over this background content, it is only
      here to demonstrate how DetailView overlays on top of other content.
    </p>
  </MemoryRouter>
);

export const EmptyState = Template.bind({});
EmptyState.args = {
  children: null,
};

export const Populated = Template.bind({});
Populated.args = {
  children: (
    <div>
      <p>
        Nest a View here. Note that DetailView modal contents require at least
        one focusable element, in order to trap focus inside the modal.
      </p>
      <Button label="Button" className="float-r" />
      <div className="clear" />
    </div>
  ),
};

export const Togglable: ComponentStory<typeof DetailView> = (args) => {
  const detailView = useDetailView();
  return (
    <MemoryRouter>
      {detailView.render(() => (
        <div>
          <p>Hello world</p>
          <Button label="Close" className="float-r" onClick={detailView.hide} />
          <div className="clear" />
        </div>
      ))}
      <h1>Background Content</h1>
      <p>
        This is a paragraph of words. Perhaps you have read words before, but
        have you read these words?
      </p>
      <Button label="Show" onClick={() => detailView.show()} />
      <p>
        Background text is in the background doing background text things. You
        aren't missing much if you skip over this background content, it is only
        here to demonstrate how DetailView overlays on top of other content.
      </p>
    </MemoryRouter>
  );
};
