import React from "react";
import { ComponentStory, ComponentMeta } from "@storybook/react";
import { Button } from "./Button";

export default {
  title: "Macrows/Primitives/Button",
  component: Button,
  argTypes: {
    onClick: { action: "onClick" },
  },
} as ComponentMeta<typeof Button>;

const Template: ComponentStory<typeof Button> = (args) => <Button {...args} />;

export const Primary = Template.bind({});
Primary.args = {
  primary: true,
  label: "Button",
};

export const Secondary = Template.bind({});
Secondary.args = {
  label: "Button",
};

export const Square = Template.bind({});
Square.args = {
  square: true,
  label: "Button",
};

export const Large = Template.bind({});
Large.args = {
  size: "large",
  label: "Button",
};

export const Small = Template.bind({});
Small.args = {
  size: "small",
  label: "Button",
};

export const Tiny = Template.bind({});
Tiny.args = {
  size: "tiny",
  label: "Button",
};

export const Disabled = Template.bind({});
Disabled.args = {
  disabled: true,
  label: "Button",
};

export const WithStateOverride = Template.bind({});
WithStateOverride.args = {
  state: "active",
  label: "Button",
};
