import React from "react";
import { ComponentStory, ComponentMeta } from "@storybook/react";

import { ListManager } from "./ListManager";

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: "Macrows/Primitives/ListManager",
  component: ListManager,
  // More on argTypes: https://storybook.js.org/docs/react/api/argtypes
} as ComponentMeta<typeof ListManager>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: ComponentStory<typeof ListManager> = (args) => (
  <ListManager {...args} />
);

export const Populated = Template.bind({});
Populated.args = {
  label: "cheetohs",
  items: [
    { name: "Fire Hot", id: "1" },
    { name: "Extra Cheesy", id: "2" },
    { name: "Original", id: "3" },
  ],
};

export const EmptyState = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
EmptyState.args = {
  label: "things",
  items: [],
};
