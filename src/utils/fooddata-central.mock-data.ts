import { SearchResults } from "./fooddata-central.types";

export const mockSearchResults: SearchResults["foods"] = [
  /**
   * Data Type: Foundation Foods
   */
  {
    fdcId: 1999631,
    description: "Almond milk, unsweetened, plain, shelf stable",
    additionalDescriptions: "",
    dataType: "Foundation",
    publishedDate: "2021-10-28",
    allHighlightFields: "",
    score: 1.0,
    foodNutrients: [
      {
        nutrientId: 1003,
        nutrientName: "Protein",
        unitName: "G",
        value: 0.555,
      },
      {
        nutrientId: 1004,
        nutrientName: "Total lipid (fat)",
        unitName: "G",
        value: 1.22,
      },
      {
        nutrientId: 1005,
        nutrientName: "Carbohydrate, by difference",
        unitName: "G",
        value: 0.337,
      },
    ],
  },

  /**
   * Data Type: SR Legacy Foods
   */
  {
    fdcId: 169814,
    description: "Agave, raw (Southwest)",
    additionalDescriptions: "",
    dataType: "SR Legacy",
    publishedDate: "2019-04-01",
    allHighlightFields: "",
    score: 1.0,
    foodNutrients: [
      {
        nutrientId: 1004,
        nutrientName: "Total lipid (fat)",
        unitName: "G",
        value: 0.15,
      },
      {
        nutrientId: 1003,
        nutrientName: "Protein",
        unitName: "G",
        value: 0.52,
      },
      {
        nutrientId: 1005,
        nutrientName: "Carbohydrate, by difference",
        unitName: "G",
        value: 16.2,
      },
    ],
  },

  /**
   * Data Type: Survey Foods (FNDDS)
   */
  {
    allHighlightFields: "",
    dataType: "Survey (FNDDS)",
    description: "Mixed salad greens, raw",
    fdcId: 1103361,
    foodNutrients: [
      {
        nutrientId: 1003,
        nutrientName: "Protein",
        unitName: "G",
        value: 1.61,
      },
      {
        nutrientId: 1004,
        nutrientName: "Total lipid (fat)",
        unitName: "G",
        value: 0.22,
      },
      {
        nutrientId: 1005,
        nutrientName: "Carbohydrate, by difference",
        unitName: "G",
        value: 2.93,
      },
    ],
    publishedDate: "2020-10-30",
    score: 498.05878,
  },

  /**
   * Data Type: Branded Foods (in grams)
   */
  {
    allHighlightFields:
      "<b>Ingredients</b>: <em>CHEDDAR</em> <em>CHEESE</em> (PASTEURIZED MILK, <em>CHEESE</em> CULTURE, SALT, ENZYMES, ANNATTO COLOR), POTATO STARCH",
    brandName: "CRYSTAL FARMS",
    brandOwner: "Three Square Inc.",
    dataSource: "LI",
    dataType: "Branded",
    description: "CHEDDAR CHEESE",
    fdcId: 2015943,
    foodNutrients: [
      {
        nutrientId: 1003,
        nutrientName: "Protein",
        unitName: "G",
        value: 25.0,
      },
      {
        nutrientId: 1004,
        nutrientName: "Total lipid (fat)",
        unitName: "G",
        value: 32.1,
      },
      {
        nutrientId: 1005,
        nutrientName: "Carbohydrate, by difference",
        unitName: "G",
        value: 3.57,
      },
    ],
    ingredients:
      "CHEDDAR CHEESE (PASTEURIZED MILK, CHEESE CULTURE, SALT, ENZYMES, ANNATTO COLOR), POTATO STARCH, STARCH AND CELLULOSE POWDER TO PREVENT CAKING, NATAMYCIN (MOLD INHIBITOR).",
    publishedDate: "2021-10-28",
    score: 866.48,
    /**
     * Note: this is a "suggested serving size".
     * It is unrelated to the foodNutrients above
     * which are normalized to 100g portions.
     *
     * (Also, not all search results will have these two fields)
     */
    servingSize: 28.0,
    servingSizeUnit: "g",
  },

  /**
   * Data Type: Branded Foods (in milliliters)
   */
  {
    fdcId: 1900168,
    description: " ALMOND MILK, ORIGINAL",
    dataType: "Branded",
    gtinUpc: "041220970063",
    publishedDate: "2021-07-29",
    brandOwner: "H E Butt Grocery Company",
    brandName: "H-E-B",
    ingredients:
      "ORGANIC ALMONDMILK (FILTERED WATER, ORGANIC ALMONDS), ORGANIC EVAPORATED CANE SYRUP, TRICALCIUM PHOSPHATE, POTASSIUM CITRATE, SEA SALT, ORGANIC LOCUST BEAN GUM, GELLAN GUM, ORGANIC SUNFLOWER LECITHIN, VITAMIN A PALMITATE, VITAMIN D2, DL-ALPHA-TOCOPHEROL ACETATE (VITAMIN E).",
    dataSource: "LI",
    servingSizeUnit: "ml",
    servingSize: 240.0,
    allHighlightFields: "",
    score: -535.16223,
    foodNutrients: [
      {
        nutrientId: 1003,
        nutrientName: "Protein",
        unitName: "G",
        value: 0.42,
      },
      {
        nutrientId: 1004,
        nutrientName: "Total lipid (fat)",
        unitName: "G",
        value: 1.04,
      },
      {
        nutrientId: 1005,
        nutrientName: "Carbohydrate, by difference",
        unitName: "G",
        value: 3.33,
      },
    ],
  },
];
