/**
 * MIT License
 *
 * Copyright (c) 2020-present Eric Liu
 */

import { abortableFetch, AbortableRequest } from "./abortable-fetch";
import {
  SearchParams,
  SearchResults,
  DetailsResults,
} from "./fooddata-central.types";

type ClientResponse<R> = {
  success: boolean;
  data?: R;
  error?: Error;
  status?: number;
};

class Client {
  private base_url = "https://api.nal.usda.gov/fdc/v1";
  private _config = { api_key: "" };

  constructor(props: { api_key?: string }) {
    if (props.api_key === undefined) {
      throw Error("api_key is required.");
    }

    this._config.api_key = props.api_key;
  }

  private uri(params?: SearchParams) {
    const api_key = `api_key=${this._config.api_key}`;

    if (params === undefined) {
      return api_key;
    }

    const query = Object.entries(params)
      .map((entry) => {
        let [key, value] = entry;

        if (Array.isArray(value)) {
          value = value.join(",");
        }

        return `${key}=${value}`;
      })
      .join("&");

    return `${api_key}&${query}`;
  }

  private call<R>(uri: string): AbortableRequest<ClientResponse<R>> {
    let status;
    const request = abortableFetch(`${this.base_url}${uri}`);
    return {
      abort: () => request.abort(),
      ready: (async () => {
        try {
          const result = await request.ready;
          status = result.status;
          const data: R = await result.json();

          return { success: true, data };
        } catch (error: any) {
          return { success: false, error, status };
        }
      })(),
    };
  }

  public search(params: SearchParams) {
    return this.call<SearchResults>(`/search?${this.uri(params)}`);
  }

  public details(fdcId: number) {
    return this.call<DetailsResults>(`/${fdcId}?${this.uri()}`);
  }
}

export default Client;
