# Macrows

Macrows is a macro-aware portion-calculator and meal prepping assistant. Use it to customize recipes and help with meal planning and preparation.

## Thick-client

All functionality is implemented client-side as a React app. Data is persisted in `localStorage`, and can also be exported + imported.

This decision was made because a) the project scope felt reasonable for a React-only solution, and b) not building a back-end service is easier than building a back-end service.

## Beta

Note that the feature-set and schema are still in development.
